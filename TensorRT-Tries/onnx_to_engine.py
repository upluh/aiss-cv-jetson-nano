import tensorrt as trt

logger = trt.Logger(trt.Logger.WARNING)
builder = trt.Builder(logger)
        
network = builder.create_network(1 << int(trt.NetworkDefinitionCreationFlag.EXPLICIT_BATCH))
parser = trt.OnnxParser(network, logger)

success = parser.parse_from_file("./yolov5_aisscv_models/runs/train/v1_0_640x_128b_300e_noStartWeightsW_v5s-model/weights/best.onnx")
for idx in range(parser.num_errors):
    print(parser.get_error(idx))

if not success:
    print("something went very wrong")
    pass # Error handling code here

config = builder.create_builder_config()
# config.set_memory_pool_limit(trt.MemoryPoolType.WORKSPACE, 1 << 20) # 1 MiB (by default it uses the total global memory size)

# serialized engine (this is saved hereafter to be used more often)
serialized_engine = builder.build_serialized_network(network, config)

with open("sample.engine", "wb") as f:
    f.write(serialized_engine)

