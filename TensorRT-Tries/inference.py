import cv2
import pycuda.driver as cuda
import pycuda.autoinit
import tensorrt as trt
import numpy as np


# path to the trt engine
ENGINE_PATH="./sample.engine"


# load the engine
def load_trt_engine(engine_path):
    with open(engine_path, "rb") as f, trt.Runtime(trt.Logger(trt.Logger.WARNING)) as runtime:
        engine = runtime.deserialize_cuda_engine(f.read())

    return engine

# inference function
def inference(context, input_img):
    # Engine input and output
    input_binding = context.get_binding_shape(0)
    output_binding = context.get_binding_shape(1)

    # preprocess img
    input_img_resized = cv2.resize(input_img, (input_binding[3], input_binding[2]))
    input_img_resized = input_img_resized.transpose((2,0,1)) # HWC to CHW
    input_img_resized = np.ascontiguousarray(input_img_resized, dtype=np.float32)
    input_img_resized /= 255.0


    # allocate device memory (input and output buffers)
    d_input = cuda.mem_alloc(input_img_resized.nbytes)
    d_output = cuda.mem_alloc(output_binding[1] * trt.float32.itemsize) 
    
    # to and from gpu
    cuda.memcpy_htod(d_input, input_img_resized)

    # infer
    context.execute_v2(bindings=[int(d_input), int(d_output)])

    # buffer for output, copy results to host
    output = np.empty(output_binding[1], dtype=np.float32)
    cuda.memcpy_dtoh(output, d_output)

    return output


def main():

    # load engine
    engine = load_trt_engine(ENGINE_PATH)

    # execution context
    ctx = engine.create_execution_context()

    # use webcam
    print("starting Video capture")
    cap = cv2.VideoCapture('nvarguscamerasrc ! video/x-raw(memory:NVMM), width=3280, height=2464, format=(string)NV12, framerate=(fraction)20/1 ! nvvidconv ! video/x-raw, width=820, height=616, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink', cv2.CAP_GSTREAMER)


    if not cap.isOpened():
        print("Error: Cannot access webcam")
        return

    # video loop
    while True:
        ret, frame = cap.read()
        if not ret:
            break

        # inference on current img
        #output = inference(ctx, frame)

        # could add more postprocessing here

        # display
        cv2.imshow("Inference", frame)

        # q to quit
        if cv2.waitKey(1) & 0xFF == ord("q"):
            break

    # clean up
    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()

# logger = trt.Logger(trt.Logger.WARNING)

# rutime = trt.Runtime(logger)

# with open("sample.engine", "rb") as f:
#    serialized_engine = f.read()
# engine = runtime.deserialize_cuda_engine(serialized_engine)
#
#    cxt = engine.create_execution_context()
#    cxt.set_tensor_address()

