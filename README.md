# Jetson Nano Inference Setup Guide

1. [Prerequisites]()
2. [Setup Instructions]()

   - [Installing the Qengineering Image](#1-installing-the-qengineering-image)
   - [Install Ultralytics](#2-install-ultralytics)
   - [Clone This Repository](#3-clone-this-repository)
   - [Navigate to the Cloned Repository](#4-navigate-to-the-cloned-repository)
   - [Run the Script](#5-run-the-script)
   - [Wait for Client Connection](#6-wait-for-client-connection)

In this README, we provide a step-by-step guide on setting up and running the inference on your Jetson Nano. By following these instructions, you will be able to smoothly run the provided scripts and initiate communication with your client application.

> **_NOTE:_** TensorRT-Tries are our script for using TensorRT. This unfortunately did not work. The generation the onnx file and the engine work according to the script. The inference script using TensorRT is work in progress. For a detailed explanation view the[Deployment documentation](https://git.scc.kit.edu/aiss_cv/documentation/-/blob/main/Deployment.md) and [Future Outlook documentation](https://git.scc.kit.edu/aiss_cv/documentation/-/blob/main/FutureOutlook.md)

## Prerequisites:

- Jetson Nano board.
- A working internet connection.

## Setup Instructions:

### 1. Installing the Qengineering Image

Before we proceed with the software, we need to ensure our Jetson Nano is running the right OS image. We recommend the Qengineering's custom Jetson Nano Ubuntu 20 image, which has been tailored for projects like ours.

- Head over to the [Qengineering's Jetson Nano Ubuntu 20 image repository](https://github.com/Qengineering/Jetson-Nano-Ubuntu-20-image).
- Follow the provided instructions to download and install the image on your Jetson Nano.

### 2. Install Ultralytics

Once your Jetson Nano is set up with the new image, it's time to install the `ultralytics` library:

```bash
pip3 install ultralytics
```

### 3. Clone This Repository

To get the necessary scripts and code, clone this repository to your Jetson Nano:

```bash
git clone <repository-url>
```

Replace `<repository-url>` with the URL of this Git repository.

### 4. Navigate to the Cloned Repository

Change your directory to the newly cloned repository:

```bash
cd <repository-name>
```

Replace `<repository-name>` with the name of the cloned repository.

### 5. Run the Script

Now, run the provided script using:

```bash
python3 app.py
```

### 6. Wait for Client Connection

Upon successful execution, you should see the terminal displaying:

```bash
⏲️  Waiting for client to connect...
```

At this point, your Jetson Nano is set and ready for the client to connect and start receiving data.

---

Thank you for using our guide! If you face any issues or have suggestions, feel free to raise an issue or submit a pull request.

---
